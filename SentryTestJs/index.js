document.getElementById('button').addEventListener('click', function () {

        const DNS = 'Public DNS here';

        console.log('Gonna send logs...');

        Raven.config(DNS).install();

        Raven.captureException(new Error('Exception occurred!'));

        try {
            Raven.context(function () {
                throw new Error('Using context');
            })
        } catch (e) {}

        const doIt = function () {
            throw new Error('Using wrap');
        };
        const w = Raven.wrap(doIt);
        try {
            w();
        } catch (e) {}

        Raven.setUserContext({
            id: 'id',
            username: 'John Smith'
        });

        Raven.captureBreadcrumb({
            message: 'Breadcrumb message'
        });

        Raven.captureMessage('Sends message', {level: 'info'});

    }
);
