package test;

import io.sentry.Sentry;
import io.sentry.SentryClient;
import io.sentry.SentryClientFactory;
import io.sentry.context.Context;
import io.sentry.event.Breadcrumb;
import io.sentry.event.BreadcrumbBuilder;
import io.sentry.event.User;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.io.IOException;

public class SentryTest {

    private static final String DNS = "DNS here";
    private static final Logger logger = LoggerFactory.getLogger(SentryTest.class);

    @Test
    public void test() {
        Sentry.init(DNS);

        logger.info("This is INFO level log.");

        MDC.put("MDC-key", "MDC context value.");

        logger.warn("Exception occurred... {}", new IOException("Failed to write something..."));

        Sentry.setUser(new User("id", "John Smith", "127.0.0.1", "johnny@example.com"));
        Sentry.record(new BreadcrumbBuilder()
                              .setLevel(Breadcrumb.Level.ERROR)
                              .setMessage("Message").build());
        Sentry.capture("From static API");

        logger.error("Error level log [{}, {}]", "P1", "P2");
    }

    @Test
    public void staticApi() {
        SentryClient sentry = SentryClientFactory.sentryClient(DNS);
        Context context = sentry.getContext();

        sentry.sendMessage("Sending message...");

        context.recordBreadcrumb(new BreadcrumbBuilder()
                                         .setLevel(Breadcrumb.Level.ERROR)
                                         .setMessage("Message").build());
        context.setUser(new User("id", "John Smith", "127.0.0.1", "johnny@example.com"));
        sentry.sendException(new IndexOutOfBoundsException("Something stupid happened."));
    }

}
